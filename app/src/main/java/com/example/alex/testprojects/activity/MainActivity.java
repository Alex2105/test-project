package com.example.alex.testprojects.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.example.alex.testprojects.tost.MyToast;
import com.example.alex.testprojects.R;
import com.example.alex.testprojects.retrofit.ResponseFromSerner;
import com.example.alex.testprojects.retrofit.Retrofit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity implements View.OnClickListener {
    public static final int SPLASH_DISPLAY_DURATION = 2000;
    private EditText code, phoneNumber;
    private Button getCode, reffirm;
    private ImageView question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view();
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 12) {
                    getCode.setEnabled(true);
                } else {
                    getCode.setEnabled(false);
                }
            }
        });

    }

    private void view() {
        phoneNumber = (EditText) findViewById(R.id.phone_number);
        code = (EditText) findViewById(R.id.code);
        getCode = (Button) findViewById(R.id.get_code);
        reffirm = (Button) findViewById(R.id.reaffirm);
        question = (ImageView) findViewById(R.id.list_countries);
        getCode.setEnabled(false);
        reffirm.setEnabled(false);
        reffirm.setOnClickListener(this);
        question.setOnClickListener(this);
        getCode.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_countries:
                phoneNumber.setText("");
                reffirm.setEnabled(false);
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(intent, 1);
                break;
            case R.id.get_code:
                MyToast.info(MainActivity.this, getString(R.string.push_informatoin));
                handler();
                reffirm.setEnabled(true);
                break;
            case R.id.reaffirm:
                Intent intentMap = new Intent(MainActivity.this, MapActivity.class);
                startActivity(intentMap);
                break;

        }
    }

    private void handler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendData(getPhoneNumber());
            }
        }, SPLASH_DISPLAY_DURATION);
    }

    @NonNull
    private String getPhoneNumber() {
        return phoneNumber.getText().toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        String number = data.getStringExtra("code");
        phoneNumber.setHint(number);


    }

    private void sendData(String phone) {
        Retrofit.sendPnoneNumberInformation(phone, new Callback<ResponseFromSerner>() {
            @Override
            public void success(ResponseFromSerner responseFromSerner, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
                MyToast.error(MainActivity.this, "Хьюстон, у нас проблемы, сервер отвечает ошибкой");


            }
        });
    }
}
