package com.example.alex.testprojects.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.alex.testprojects.R;
import com.example.alex.testprojects.adapter.RecyclerAdapter;
import com.example.alex.testprojects.adapter.RecyclerItemClickListener;

/**
 * Created by Alex on 2/18/2017.
 */

public class ListActivity extends Activity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String ukrainianNumber, russianNumber, usaNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        String[] arraysCountryNumber = getStringArray();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new RecyclerAdapter(arraysCountryNumber);
        recyclerView.setAdapter(adapter);
        ukrainianNumber = "+38(***) *** ** **";
        russianNumber = "+7(***) *** ** **";
        usaNumber = "+1(***) *** ** **";

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                switch (i) {
                    case 0:
                        broadcastData(ukrainianNumber);
                        break;
                    case 1:
                        broadcastData(russianNumber);
                        break;

                    case 2:
                        broadcastData(usaNumber);
                        break;
                }
            }
        }));
    }

    private void broadcastData(String codeCountries) {
        Intent intent = new Intent(ListActivity.this, MainActivity.class);
        intent.putExtra("code", codeCountries);
        setResult(RESULT_OK, intent);
        finish();
    }


    private String[] getStringArray() {
        return getResources().getStringArray(R.array.countries);
    }
}
