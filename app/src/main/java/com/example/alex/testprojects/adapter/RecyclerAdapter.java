package com.example.alex.testprojects.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alex.testprojects.R;

/**
 * Created by Alex on 2/18/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private String[] arraysCountryNumber;

    public RecyclerAdapter(String[] dataset) {
        arraysCountryNumber = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.countyNumber.setText(arraysCountryNumber[position]);
    }

    @Override
    public int getItemCount() {
        return arraysCountryNumber.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView countyNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            countyNumber = (TextView) itemView.findViewById(R.id.country_number_item);
        }
    }
}
