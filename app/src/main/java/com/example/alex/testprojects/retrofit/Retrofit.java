package com.example.alex.testprojects.retrofit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Alex on 2/18/2017.
 */

public class Retrofit {
    private static final String ENDPOINT = "http://s3.logist.ua/testdata";
    private static PhoneNumber number;

    static {
        init();
    }

    private static void init() {
        RestAdapter postAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        number = postAdapter.create(PhoneNumber.class);
    }

    public static void sendPnoneNumberInformation(String phoneNumber, Callback<ResponseFromSerner> callback) {
        number.setPhoneNumber(phoneNumber, callback);
    }


    interface PhoneNumber {
        @POST("/data.php")
        void setPhoneNumber(@Query("data") String phoneNumber, Callback<ResponseFromSerner> callback);
    }
}
